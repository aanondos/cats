package routes

import (
	"net/http"

	"github.com/Anondo/cats/api"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

var e *echo.Echo

func init() {
	e = echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.CORS())
	e.Pre(middleware.RemoveTrailingSlash())

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{http.MethodGet, http.MethodHead, http.MethodPut, http.MethodPatch, http.MethodPost, http.MethodDelete, http.MethodOptions},
	}))

}

// Router list all the routes used by this app
func Router() *echo.Echo {

	/*
		#####################
		### API V1 routes ###
		#####################
	*/

	// API groups
	v1 := e.Group("/api/v1")
	cats := v1.Group("/cats")

	/*
		####################
		### cats routes  ###
		####################
	*/

	cats.GET("", api.GetCats())
	cats.GET("/:id", api.GetCat())
	cats.POST("", api.CreateCat())
	cats.DELETE("/:id", api.RemoteCat())
	cats.PUT("/:id", api.UpdateCat())

	return e
}
