package servers

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/Anondo/cats/configs"
	"github.com/Anondo/cats/routes"
)

// StartServer starts the http server
func StartServer() {
	e := routes.Router()

	appCfg := configs.App()

	portStr := fmt.Sprintf(":%d", appCfg.Port)

	server := &http.Server{
		ReadTimeout:  appCfg.ReadTimeout,
		WriteTimeout: appCfg.WriteTimeout,
		IdleTimeout:  appCfg.IdleTimeout,
		Addr:         portStr,
		Handler:      e,
	}

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, syscall.SIGKILL, syscall.SIGINT, syscall.SIGQUIT)

	go func() {
		if err := server.ListenAndServe(); err != nil {
			log.Fatal(err.Error())
		}
	}()

	log.Printf("Listening on port::%d...\n", appCfg.Port)
	<-stop

	log.Println("Shutting down server...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	_ = server.Shutdown(ctx)

	log.Println("Server shutdowns gracefully")

}
