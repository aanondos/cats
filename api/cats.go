package api

import (
	"net/http"
	"strconv"

	"github.com/Anondo/cats/database"
	"github.com/Anondo/cats/database/repos"
	"github.com/labstack/echo/v4"
)

// GetCats ...
func GetCats() echo.HandlerFunc {
	return func(c echo.Context) error {

		cats, err := repos.NewCatRepo(database.DB).GetCats()

		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{
				"message": "Something went wrong",
				"error":   err.Error(),
			})
		}

		return c.JSON(http.StatusOK, cats)
	}
}

// GetCat ...
func GetCat() echo.HandlerFunc {
	return func(c echo.Context) error {

		catID, _ := strconv.Atoi(c.Param("id"))

		cat, err := repos.NewCatRepo(database.DB).GetCat(catID)

		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{
				"message": "Something went wrong",
				"error":   err.Error(),
			})
		}

		return c.JSON(http.StatusOK, cat)
	}
}

// CreateCatRequest ...
type CreateCatRequest struct {
	Name  string `json:"name"`
	Color string `json:"color"`
}

// CreateCat ...
func CreateCat() echo.HandlerFunc {
	return func(c echo.Context) error {

		req := CreateCatRequest{}

		if err := c.Bind(&req); err != nil {
			if err != nil {
				return c.JSON(http.StatusBadRequest, map[string]string{
					"message": "Bad request",
					"error":   err.Error(),
				})
			}
		}

		if _, err := repos.NewCatRepo(database.DB).CreateCat(req.Name, req.Color); err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{
				"message": "Something went wrong",
				"error":   err.Error(),
			})
		}

		return c.JSON(http.StatusOK, map[string]string{
			"message": "created",
			"error":   "",
		})
	}
}

// RemoteCat ...
func RemoteCat() echo.HandlerFunc {
	return func(c echo.Context) error {

		catID, _ := strconv.Atoi(c.Param("id"))

		if err := repos.NewCatRepo(database.DB).RemoveCat(catID); err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{
				"message": "Something went wrong",
				"error":   err.Error(),
			})
		}

		return c.JSON(http.StatusOK, map[string]string{
			"message": "deleted",
			"error":   "",
		})
	}
}

// UpdateCatRequest ...
type UpdateCatRequest struct {
	Name  string `json:"name"`
	Color string `json:"color"`
}

// UpdateCat ...
func UpdateCat() echo.HandlerFunc {
	return func(c echo.Context) error {

		req := UpdateCatRequest{}

		if err := c.Bind(&req); err != nil {
			if err != nil {
				return c.JSON(http.StatusBadRequest, map[string]string{
					"message": "Bad request",
					"error":   err.Error(),
				})
			}
		}

		catID, _ := strconv.Atoi(c.Param("id"))

		if err := repos.NewCatRepo(database.DB).UpdateCat(catID, req.Name, req.Color); err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{
				"message": "Something went wrong",
				"error":   err.Error(),
			})
		}

		return c.JSON(http.StatusOK, map[string]string{
			"message": "updated",
			"error":   "",
		})

	}
}
