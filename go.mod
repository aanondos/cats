module github.com/Anondo/cats

go 1.12

require (
	github.com/jinzhu/gorm v1.9.14
	github.com/labstack/echo/v4 v4.1.16
	github.com/spf13/viper v1.7.0
)
