package database

import (
	"fmt"

	"github.com/jinzhu/gorm"
	// mysql dialects need to be imported separately
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

// DB ...
var DB *gorm.DB

// Connect ...
func Connect(username, password, host, db string, port int) error {
	uri := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?parseTime=True", username, password, host, port, db)
	d, err := gorm.Open("mysql", uri)

	if err != nil {
		return err
	}

	DB = d.LogMode(true).Debug()

	return nil

}
