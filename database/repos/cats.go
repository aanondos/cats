package repos

import (
	"github.com/Anondo/cats/database/models"
	"github.com/jinzhu/gorm"
)

// CatRepo ...
type CatRepo struct {
	db *gorm.DB
}

// NewCatRepo ...
func NewCatRepo(db *gorm.DB) Cat {
	return &CatRepo{db: db}
}

// GetCats ...
func (c *CatRepo) GetCats() ([]models.Cat, error) {
	cats := []models.Cat{}

	if err := c.db.Find(&cats).Error; err != nil {
		return nil, err
	}

	return cats, nil
}

// GetCat ...
func (c *CatRepo) GetCat(catID int) (*models.Cat, error) {
	cat := models.Cat{}

	if err := c.db.First(&cat, catID).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, nil
		}

		return nil, err
	}

	return &cat, nil
}

// CreateCat ...
func (c *CatRepo) CreateCat(name, color string) (int, error) {

	newCat := models.Cat{
		Name:  name,
		Color: color,
	}

	err := c.db.Create(&newCat).Error

	if err != nil {
		return 0, err
	}

	return newCat.ID, nil
}

// RemoveCat ...
func (c *CatRepo) RemoveCat(catID int) error {

	if err := c.db.Where("id = ?", catID).Delete(&models.Cat{}).Error; err != nil {
		return err
	}

	return nil
}

// UpdateCat ...
func (c *CatRepo) UpdateCat(catID int, name, color string) error {

	err := c.db.Table("cat").Where("id = ?", catID).Updates(map[string]interface{}{
		"name":  name,
		"color": color,
	}).Error

	if err != nil {
		return err
	}

	return nil
}
