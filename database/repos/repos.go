package repos

import "github.com/Anondo/cats/database/models"

// Cat ...
type Cat interface {
	GetCats() ([]models.Cat, error)
	GetCat(int) (*models.Cat, error)
	CreateCat(string, string) (int, error)
	RemoveCat(int) error
	UpdateCat(int, string, string) error
}
