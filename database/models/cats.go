package models

// Cat ...
type Cat struct {
	ID    int    `gorm:"column:id;primary_key" json:"id"`
	Name  string `gorm:"column:name" json:"name"`
	Color string `gorm:"column:color" json:"color"`
}

// TableName sets the insert table name for this struct type
func (c *Cat) TableName() string {
	return "cat"
}
