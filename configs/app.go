package configs

import (
	"time"

	"github.com/spf13/viper"
)

// ApplicationConfig holds the application configuration
type ApplicationConfig struct {
	Port         int
	ReadTimeout  time.Duration
	WriteTimeout time.Duration
	IdleTimeout  time.Duration
}

// appCfg is the default application configuration
var appCfg ApplicationConfig

// App returns the default application configuration
func App() ApplicationConfig {
	return appCfg
}

// LoadApp loads application configuration
func LoadApp() {
	appCfg = ApplicationConfig{
		Port:         viper.GetInt("app.port"),
		ReadTimeout:  viper.GetDuration("app.read_timeout") * time.Second,
		WriteTimeout: viper.GetDuration("app.write_timeout") * time.Second,
		IdleTimeout:  viper.GetDuration("app.idle_timeout") * time.Second,
	}
}
