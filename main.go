package main

import (
	"log"

	"github.com/Anondo/cats/database"
	"github.com/Anondo/cats/servers"
)

func main() {

	if err := database.Connect("root", "secret", "127.0.0.1", "catdb", 3306); err != nil {
		log.Fatal(err.Error())
	}

	servers.StartServer()

}
