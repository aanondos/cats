# Cats

All about cats

## Run me (on terminal)

 1. ```$ chmod a+x ./run.sh```
 1. ```./run.sh```


Or, just follow everything done on run.sh manually.


## Endpoints

1. Fetch all cats -- **URI**: ``/api/v1/cats``, **METHOD**: ``GET``
1. Fetch a cat -- **URI**: ``/api/v1/cats/<id>``, **METHOD**: ``GET``
1. Remove a cat -- **URI**: ``/api/v1/cats/<id>``, **METHOD**: ``DELETE``
1. Update a cat -- **URI**: ``/api/v1/cats/<id>``, **METHOD**: ``PUT``
1. Create a cat -- **URI**: ``/api/v1/cats``, **METHOD**: ``POST``

## Requirements

1. Run the SQL migration using the given sql migration file
1. docker-compose(optional): docker-compose was used in this project just to boot up the database with ease.
1. My-SQL instance with Databse named 'catdb'(if no docker-compose)
