#! /bin/sh




# booting up dependecy containers
docker-compose up -d

# Build go program
echo "building cats API ..."
export GO111MODULE=on
CGO_ENABLED=0 GOFLAGS=-mod=vendor go build


# Run the app
./cats
